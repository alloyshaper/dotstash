use std::{io, path::PathBuf};

use snafu::Snafu;

#[derive(Snafu, Debug)]
#[snafu(visibility(pub(crate)))]
pub(crate) enum ConfigError {
    #[snafu(display("Unable to read configuration from `{}`", path.display()))]
    Read { source: io::Error, path: PathBuf },
    #[snafu(display("Unsupported configuration format `{}`.\nSupported formats: YAML, JSON.\nPlease use one of the supported formats for the configuration file.", ext))]
    UnsupportedFormat { ext: String },
}
