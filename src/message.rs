use std::fmt::Debug;

use dyn_clone::DynClone;
use dyn_partial_eq::dyn_partial_eq;
use property::Property;
use serde::Deserialize;

use crate::plugins::style::ArchStyle;

/// An interface for a style of a message.
#[typetag::serde(tag = "name")]
#[dyn_partial_eq]
pub trait MessageStyle: Debug + DynClone {
    fn msg(&self) -> &str;
    fn info(&self) -> &str;
    fn ask(&self) -> &str;
    fn warning(&self) -> &str;
    fn error(&self) -> &str;
}

dyn_clone::clone_trait_object!(MessageStyle);

/// Represents a message.
#[derive(Deserialize, Property)]
#[property(get(crate), set(disable), mut(disable))]
pub struct Message {
    source: String,
    #[property(skip)]
    kind: MessageKind,
}

impl Message {
    pub fn new(
        source: String,
        kind: MessageKind,
    ) -> Self {
        Self {
            source,
            kind,
        }
    }
}

/// Manages alerts with a specific message style.
pub struct AlertManager {
    pub(crate) style: Box<dyn MessageStyle>,
}

impl AlertManager {
    pub fn new(style: Box<dyn MessageStyle>) -> Self {
        Self {
            style,
        }
    }

    /// Notifies the user with the specified message.
    pub fn notify(
        &self,
        message: &Message,
    ) {
        match message.kind {
            MessageKind::Info => println!("{}{}", self.style.msg(), message.source()),
            MessageKind::Message => println!("{}{}", self.style.msg(), message.source()),
            MessageKind::Ask => println!("{}{}", self.style.ask(), message.source()),
            MessageKind::Warning => println!("{}{}", self.style.warning(), message.source()),
            MessageKind::Error => println!("{}{}", self.style.error(), message.source()),
        }
    }
}

impl<'a> Default for AlertManager {
    fn default() -> Self {
        Self {
            style: Box::new(ArchStyle),
        }
    }
}

/// Represents the kind of a message.
#[derive(Deserialize)]
pub enum MessageKind {
    /// Indicates an informational message.
    Info,
    /// Indicates a regular message.
    Message,
    /// Indicates a question or inquiry message.
    Ask,
    /// Indicates a warning message.
    Warning,
    /// Indicates an error message.
    Error,
}
