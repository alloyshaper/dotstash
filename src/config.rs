use std::{fs, path::PathBuf};

use serde::{Deserialize, Serialize};
use snafu::prelude::*;

use crate::{
    error::{ConfigError::UnsupportedFormat, ReadSnafu},
    message::{AlertManager, Message, MessageKind},
    plugins::Plugin,
};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
#[serde(transparent)]
/// Represents the configuration data for the CLI application.
pub struct ConfigData {
    #[serde(flatten)]
    pub(crate) plugins: Vec<Box<dyn Plugin>>,
}

impl ConfigData {
    /// Reads the configuration data from the specified file path and returns
    /// Self.
    pub fn from_path(path: &PathBuf) -> Result<Self, anyhow::Error> {
        let content = fs::read_to_string(path).context(ReadSnafu {
            path,
        })?;
        match path.extension().and_then(|os_val| os_val.to_str()) {
            Some("json") => Ok(serde_json::from_str::<Self>(&content)?),
            Some("yaml" | "yml") => Ok(serde_yaml::from_str::<Self>(&content)?),
            _ext => {
                Err(UnsupportedFormat {
                    ext: _ext.unwrap_or_default().to_owned(),
                }
                .into())
            },
        }
    }
}

/// Represents a dispatcher for configuration data and launching plugins.
pub struct ConfigDispatcher<'a> {
    data: &'a ConfigData,
    manager: &'a mut AlertManager,
}

impl<'a> ConfigDispatcher<'a> {
    pub fn new(
        data: &'a ConfigData,
        manager: &'a mut AlertManager,
    ) -> Self {
        Self {
            data,
            manager,
        }
    }

    /// Launches the plugins specified in the config data.
    pub fn launch_plugins(&mut self) -> Result<(), anyhow::Error> {
        for plugin in &self.data.plugins {
            if let Err(e) = plugin.execute(self.manager) {
                self.manager
                    .notify(&Message::new(e.to_string(), MessageKind::Error));
            }
        }
        Ok(())
    }
}
