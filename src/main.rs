/// A module to work with cli.
mod cli;
/// A module to work with configuration file.
mod config;
/// A module that provides error types.
mod error;
/// A module to work with messages.
mod message;
/// A module that provides plugins for configuration.
mod plugins;

use anyhow::Result;
use clap::Parser;
use cli::Cli;

fn main() -> Result<(), anyhow::Error> {
    let args = Cli::parse();
    args.handle()
}
