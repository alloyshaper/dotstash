use std::{io, path::PathBuf};

use anyhow::Result;
use clap::{value_parser, CommandFactory, Parser, Subcommand};
use clap_complete::{generate, Shell};

use crate::{
    config::{ConfigData, ConfigDispatcher},
    message::AlertManager,
};

#[derive(Parser)]
#[command(author, version,styles=get_styles())]
#[clap(arg_required_else_help = true)]
/// A command-line tool for fast managing your dotfiles
pub struct Cli {
    #[clap(subcommand)]
    subcommand: Option<CliCommand>,
}

impl Cli {
    /// Handles the received command.
    pub fn handle(&self) -> Result<(), anyhow::Error> {
        match &self.subcommand {
            Some(CliCommand::Completions {
                shell,
            }) => {
                Ok(generate(
                    shell.to_owned(),
                    &mut Cli::command(),
                    "dst",
                    &mut io::stdout().lock(),
                ))
            },
            Some(CliCommand::Run {
                path,
            }) => {
                let data = ConfigData::from_path(path)?;
                let mut manager = AlertManager::default();
                let mut dispatcher = ConfigDispatcher::new(&data, &mut manager);
                Ok(dispatcher.launch_plugins()?)
            },
            None => Ok(()),
        }
    }
}

#[derive(Subcommand)]
/// Represents the available commands for the CLI application.
enum CliCommand {
    /// Executes the configuration file
    Run {
        #[clap(value_name = "FILE", global = false, required = false, value_parser = value_parser!(PathBuf))]
        /// The path to the configuration file that will be executed
        path: PathBuf,
    },
    /// Generate completion scripts for your shell
    Completions {
        #[clap(value_enum)]
        shell: Shell,
    },
}

/// Returns the styles that will be applied to the CLI.
fn get_styles() -> clap::builder::Styles {
    clap::builder::Styles::styled()
        .usage(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(196, 153, 243)))),
        )
        .header(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(255, 121, 198)))),
        )
        .literal(
            anstyle::Style::new()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(80, 250, 123)))),
        )
        .invalid(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(255, 85, 85)))),
        )
        .error(
            anstyle::Style::new()
                .bold()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(255, 85, 85)))),
        )
        .valid(
            anstyle::Style::new()
                .bold()
                .underline()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(80, 250, 123)))),
        )
        .placeholder(
            anstyle::Style::new()
                .fg_color(Some(anstyle::Color::Rgb(anstyle::RgbColor(241, 250, 140)))),
        )
}
