use std::fmt::Debug;
mod git;
pub mod style;

use crate::message::AlertManager;

#[typetag::serde]
pub trait Plugin: Debug {
    fn execute(
        &self,
        manager: &mut AlertManager,
    ) -> Result<(), anyhow::Error>;
}
