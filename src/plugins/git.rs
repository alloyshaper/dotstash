use std::collections::BTreeMap;

use anyhow::anyhow;
use git2::Repository;
use serde::{Deserialize, Serialize};

use super::Plugin;
use crate::message::{AlertManager, Message, MessageKind};

#[derive(Debug, Serialize, Deserialize)]
pub struct GitPlugin(BTreeMap<String, GitBody>);

#[typetag::serde(name = "git")]
impl Plugin for GitPlugin {
    fn execute(
        &self,
        manager: &mut AlertManager,
    ) -> Result<(), anyhow::Error> {
        for (path, body) in self.0.iter() {
            let msg = match &body.description {
                Some(msg) => Message::new(msg.into(), MessageKind::Info),
                None => {
                    match body.method {
                        GitMethod::Clone => {
                            Message::new(format!("Cloning into '{}'...", path), MessageKind::Info)
                        },
                        GitMethod::Pull => Message::new(format!("Pull repo..."), MessageKind::Info),
                    }
                },
            };
            manager.notify(&msg);
            let _repo = match body.method {
                GitMethod::Clone => {
                    Repository::clone(body.url.as_str(), path)?;
                },
                GitMethod::Pull => {
                    let repo = Repository::open(path)?;
                    repo.find_remote("origin")?
                        .fetch(&[body.branch.clone()], None, None)?;
                    let fetch_head = repo.find_reference("FETCH_HEAD")?;
                    let fetch_commit = repo.reference_to_annotated_commit(&fetch_head)?;
                    let analysis = repo.merge_analysis(&[&fetch_commit])?;
                    if analysis.0.is_up_to_date() {
                        Ok(())
                    } else if analysis.0.is_fast_forward() {
                        let refname = format!("refs/heads/{}", body.branch);
                        let mut reference = repo.find_reference(&refname)?;
                        reference.set_target(fetch_commit.id(), "Fast-Forward")?;
                        repo.set_head(&refname)?;
                        repo.checkout_head(Some(git2::build::CheckoutBuilder::default().force()))
                    } else {
                        Err(anyhow!("Fast-Forward only!"))?
                    }?
                },
            };
        }
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(deny_unknown_fields)]
pub struct GitBody {
    url: String,
    description: Option<String>,
    branch: String,
    commit: Option<String>,
    #[serde(default)]
    method: GitMethod,
    #[serde(default = "default_force")]
    force: bool,
    #[serde(default = "default_quiet")]
    quiet: bool,
}

const fn default_force() -> bool {
    false
}

const fn default_quiet() -> bool {
    false
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "lowercase")]
pub enum GitMethod {
    Clone,
    Pull,
}

impl Default for GitMethod {
    fn default() -> Self {
        Self::Clone
    }
}
