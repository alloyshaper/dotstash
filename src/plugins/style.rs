use color_print::cstr;
use dyn_partial_eq::DynPartialEq;
use serde::{Deserialize, Serialize};

use super::Plugin;
use crate::message::{AlertManager, MessageStyle};

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq)]
pub struct StylePlugin(StyleVariant);

#[typetag::serde(name = "style")]
impl Plugin for StylePlugin {
    fn execute(
        &self,
        manager: &mut AlertManager,
    ) -> Result<(), anyhow::Error> {
        let style: Box<dyn MessageStyle> = match &self.0 {
            StyleVariant::Style(style) => style.clone(),
            StyleVariant::Custom(custom) => Box::new(custom.clone()),
        };
        manager.style = style;
        Ok(())
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Clone)]
#[serde(untagged)]
pub enum StyleVariant {
    Style(Box<dyn MessageStyle>),
    Custom(CustomStyle),
}

#[derive(Serialize, Deserialize, Debug, Clone, DynPartialEq, PartialEq)]
pub struct ArchStyle;

#[typetag::serde(name = "arch")]
impl MessageStyle for ArchStyle {
    fn msg(&self) -> &str {
        cstr!("<bold,green>==></> ")
    }

    fn info(&self) -> &str {
        cstr!("<bold,blue>  -></> ")
    }

    fn ask(&self) -> &str {
        cstr!("<bold,blue>::</> ")
    }

    fn warning(&self) -> &str {
        cstr!("<bold,yellow>==> WARNING:</> ")
    }

    fn error(&self) -> &str {
        cstr!("<bold,red>==> ERROR:</> ")
    }
}

#[derive(Serialize, Deserialize, Debug, Clone, DynPartialEq, PartialEq)]
/// Represents a style with a custom settings.
pub struct CustomStyle {
    #[serde(alias = "msg", alias = "note", alias = "m")]
    pub message: String,
    #[serde(alias = "details", alias = "i")]
    pub info: String,
    #[serde(alias = "query", alias = "inquire", alias = "q")]
    pub ask: String,
    #[serde(alias = "warn", alias = "caution", alias = "w")]
    pub warning: String,
    #[serde(alias = "err", alias = "mistake", alias = "e")]
    pub error: String,
}

impl MessageStyle for CustomStyle {
    fn msg(&self) -> &str {
        &self.message
    }

    fn info(&self) -> &str {
        &self.info
    }

    fn ask(&self) -> &str {
        &self.ask
    }

    fn warning(&self) -> &str {
        &self.warning
    }

    fn error(&self) -> &str {
        &self.error
    }

    fn typetag_name(&self) -> &'static str {
        unimplemented!()
    }

    fn typetag_deserialize(&self) {
        unimplemented!()
    }
}

#[cfg(test)]
mod tests {
    use pretty_assertions::assert_eq;
    use serde::{Deserialize, Serialize};

    use super::{ArchStyle, CustomStyle, StyleVariant};

    #[derive(Serialize, Deserialize, Debug, PartialEq)]
    struct Welcome {
        style: StyleVariant,
    }
    #[test]
    fn test_parse_style() {
        let yaml_s = "
        - style:
            name: arch
        - style:
            message: msg
            info: info
            ask: ask
            warning: warning
            error: error
        ";
        let json_s = serde_json::json!([
        {
            "style":{
                "name":"arch"
            }
        },
        {
            "style":{
                "message":"msg",
                "info":"info",
                "error":"error",
                "warning":"warning",
                "ask":"ask"
            }
        }
        ]);
        let yaml_fact: Vec<Welcome> = serde_yaml::from_str(&yaml_s).unwrap();
        let json_fact: Vec<Welcome> = serde_json::from_value(json_s).unwrap();
        let expected: Vec<Welcome> = vec![
            Welcome {
                style: StyleVariant::Style(Box::new(ArchStyle)),
            },
            Welcome {
                style: StyleVariant::Custom(CustomStyle {
                    ask: "ask".into(),
                    message: "msg".into(),
                    info: "info".into(),
                    error: "error".into(),
                    warning: "warning".into(),
                }),
            },
        ];
        assert_eq!(expected, yaml_fact);
        assert_eq!(expected, json_fact);
        assert_eq!(json_fact, yaml_fact);
    }
}
