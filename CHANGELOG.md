## v0.3.0 (2024-01-27)

### Refactor

- **Cargo.toml**: add new dependencies
- **cli**: add styles to cli, add handle func for args
- **main**: add mod(s), change let cli to args
- add missing docs for funcs
- **error.rs**: change `Error` to `ConfigError`
- **config**: match extension

### Features

- **message**: add message style interface for styling messages
- **git**: add git plugin for cloning and pulling repo(s)
- **style**: add style plugin for messages
- **plugins**: add plugin interface
- **config**: add config dispatcher for plugins

## v0.2.0 (2024-01-06)

### Refactor

- **.gitignore**: Update .gitignore
- **cz**: Update config

### Features

- Add shell plugin, new command, errors, config
- **cli**: Add completions command
